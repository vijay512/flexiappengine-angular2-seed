# Appengine sample for Google App Engine Flexible Environment - with Angular2 as UI framework
This sample demonstrates how to deploy an application on Google App Engine

## Running locally
    $ mvn jetty:run

## Deploying
    $ mvn gcloud:deploy
